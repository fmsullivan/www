---
title: Projects and Virtual Class from Lowell Makes
date: 2020-3-15 19:12:00
# updated: 2020-3-14
categories:
    - COVID-19
    - Resource
tags:
  - resource
  - education-resource
  - covid-19
  - covid-19-resource
source-org: lowell-makes
provider: lowell-makes
---

link:https://lowellmakes.com[Lowell Makes], the downtown Makerspace in Lowell, is preparing virtual classes and activities to reduce loneliness and anxiety.

Additionally, Lowell Makes has prepared a list of activities, crafts, and instructions to learn new skills.

link:https://lowellmakes.com/making-community/[Making Community, Lowell Makes]