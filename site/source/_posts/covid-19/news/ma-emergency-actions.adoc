---
title: Massachuetts closes schools, restaurants for 3 weeks
date: 2020-3-15 18:30:00
updated: 2020-3-16 13:45:00
categories:
  - COVID-19
  - News
tags:
  - news
  - covid-19-news
  - covid-19
  - state-wide
  - school
  - restaurant
source-org: mass.gov
---

== Summary

Starting on March 17th, and continuing until April 6th:

- All elementary and secondary schools are suspended
- Gatherings are limited to 25 people
- Restaurants cannot serve food, but can offer take-out and delivery

Additionally:

- All medical insurances must cover telehealth services
- Insurers must cover COVID-19 treatment and testing without co-pays or coinsurance
- Assisted living residences are to ban visitors
- Hospitals must screen visitors
- Hospitals must cancel non-essential elective procedures
- The RMV is extending expiration dates for licenses that would expire between March 1st, 2020 and April 30th, 2020

link:https://www.mass.gov/news/baker-polito-administration-announces-emergency-actions-to-address-covid-19[Press Release]

link:https://www.mass.gov/doc/march-15-2020-school-closure-order/download[School closure order (PDF)]

link:https://www.mass.gov/doc/march-15-2020-large-gatherings-25-and-restaurants-order/download[Gatherings and restaurant restrictions order (PDF)]

link:https://www.mass.gov/doc/march-15-2020-telehealth-order/download[Telehealth order (PDF)]