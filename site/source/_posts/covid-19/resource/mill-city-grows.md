---
title: Mill City Grows - fresh produce
date: 2020-3-22 
# updated: 2020-3-22
categories:
    - COVID-19
    - Resource
tags:
  - resource
  - food-resource
  - covid-19
  - covid-19-resource
source-org: mill-city-grows
provider: mill-city-grows
---

Mill City Grows is committed to the health and wellness of our community through providing locally sourced produce in this time of uncertainty. They are now accepting online orders from the public, and prioritizing orders from those in need due to limited supply. This system was developed with safety in mind, and takes all possible precautions to protect staff and customers.

See the linked order form for additional details.

[Mill City Grows Mobile Market orders](https://www.millcitygrows.org/mill-city-grows-markets/food-access/)
