---
title: COVID-19 Fact Sheet in Multiple Languages
date: 2020-3-20 18:50:00
# updated: 2020-3-16 14:00:00
categories:
  - COVID-19
  - News
tags:
  - news
  - covid-19-news
  - covid-19
---
:english: link:https://en.hesperian.org/hhg/Coronavirus[English]
:spanish: link:https://es.hesperian.org/hhg/Coronavirus[Spanish]
:french: link:https://fr.hesperian.org/hhg/Le_coronavirus[French]
:chinese: link:https://zh.hesperian.org/hhg/%E5%86%A0%E7%8A%B6%E7%97%85%E6%AF%92[Chinese]
:urdu: link:https://ur.hesperian.org/hhg/کرونا_وائرس[Urdu]
:bangla: link:https://bn.hesperian.org/hhg/করোনাভাইরাস[Bangla]
:filipino: link:https://fil.hesperian.org/hhg/Coronavirus[Filipino]
:vietnamese: link:https://vi.hesperian.org/hhg/Vi-rút_corona[Vietnamese]
:bahasa: link:https://id.hesperian.org/hhg/Virus_Corona[Bahasa Indonesia]
:farsi: link:https://fa.hesperian.org/hhg/کورونا_ویروس[Farsi]
:sindhi: link:https://sd.hesperian.org/hhg/ڪوروناوائرس[Sindhi]
:telugu: link:https://te.hesperian.org/hhg/కరోనావైరస్[Telugu]
:hindi: link:https://hi.hesperian.org/hhg/कोरोनावायरस_रोग[Hindi]

A COVID-19 Fact Sheet is available in {english}, {spanish}, {french}, {chinese}, {urdu}, {bangla}, {filipino}, {vietnamese}, {bahasa}, {farsi}, {sindhi}, {telugu}, and {hindi}.