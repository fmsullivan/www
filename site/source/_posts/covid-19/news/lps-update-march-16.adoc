---
title: Lowell Public Schools expand lunch sites
date: 2020-3-18 9:20:00
# updated: 2020-3-14
categories:
  - COVID-19
  - News
tags:
  - news
  - covid-19-news
  - covid-19
  - lowell-public-schools
source-org: lowell-public-schools
---

Lunch will now be provided at a total of 9 sites around lowell during weekdays.

All students ages 2-18 are eligible for the lunch program.

Students don't have to be present to pick up a meal, a parent or neighbor can pick up the student's lunch.

> The times and locations are subject to change, so please check the Lowell Public Schools website or Facebook/Twitter pages for any updates.

link:https://www.lowell.k12.ma.us/cms/lib/MA01907636/Centricity/Domain/2728/LPSD%20COVID-19%20Update%203-16-20.pdf[Press Release (PDF)]

link:/covid-19/resource/grab-and-go-meals/[More information about Lowell Public School grab-and-go meals]