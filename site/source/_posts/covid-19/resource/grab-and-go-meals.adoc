---
title: Lowell Public Schools Grab and Go Meals
date: 2020-3-15 9:05:00
# updated: 2020-3-14
categories:
    - COVID-19
    - Resource
tags:
  - resource
  - food-resource
  - covid-19
  - covid-19-resource
  - lowell-public-schools
source-org: lowell-public-schools
provider: lowell-public-schools
---

Lowell Public Schools has announced the schedule and locations where lunch will be provided for students.

Locations: Bartlett, Butler, Greenhalge, Lincoln, Moody, Murkland, Pawtucketville, Robinson, and STEM (Rogers)

See the linked schedule for the exact locations and times when food is available.

link:https://www.lowell.k12.ma.us/cms/lib/MA01907636/Centricity/Domain/2728/Grab%20and%20Go%20Flyer%20web.pdf[Locations and Schedule (PDF)]