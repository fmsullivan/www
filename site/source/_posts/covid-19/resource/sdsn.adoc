---
title: Social Distancing Support Network (Facebook Group)
date: 2020-3-17 13:28:00
# updated: 2020-3-14
categories:
    - COVID-19
    - Resource
tags:
  - resource
  - social-resource
  - covid-19
  - covid-19-resource
provider: social-distancing-support-network
---

> We are here to build a community to support us through our Social Distancing Efforts. Play games, make friends, learn things. Together we are strong.

link:https://www.facebook.com/groups/196468648349430/[Social Distancing Support Network on Facebook]