---
title: About Llama
permalink: group/
date: 2020-03-14 12:00:06
---

Llama helps connect Lowell citizens with each other,
both through this website and the offer/request forms.
