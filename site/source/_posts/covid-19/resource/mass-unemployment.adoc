---
title: Unemployment Pay for COVID-19
date: 2020-3-18 9:10:00
# updated: 2020-3-16 14:00:00
categories:
    - COVID-19
    - Resource
tags:
  - resource
  - financial-resource
  - unemployment
  - covid-19
  - covid-19-resource
provider: mass.gov
source-org: mass.gov
---

The Department of Unemployment Assistance has released additional information about collecting unemployment for workers affected by COVID-19.



> EOLWD and DUA also filed emergency regulations that allow people impacted by COVID-19 to collect unemployment benefits if their workplace is shut down and expects to reopen. This applies to all employees (full and part time) who are impacted by such shutdowns. link:https://www.mass.gov/how-to/apply-for-unemployment-benefits[Claimants are urged to file unemployment claims online].

> The following conditions apply to temporary shutdowns: +
> - Workers must remain in contact with their employers during the shutdown. +
> - Workers must be available for any work their employer may have for them that they are able to do. +
> - An employer may request to extend the period of the covered shutdown to 8 weeks, and workers will remain eligible for the longer period under the same conditions described above. +
> - If necessary, DUA may extend these time periods for workers and employers.

> Employers who have been paying into the system for themselves (are receiving a W-2) are able to apply as well. 


link:https://www.mass.gov/info-details/massachusetts-covid-19-unemployment-information[mass.gov: Important employer and employee information related to COVID-19]