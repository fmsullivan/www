---
title: Lowell General Hospital is Accepting Donations
date: 2020-3-20 18:50:00
# updated: 2020-3-16 14:00:00
categories:
  - COVID-19
  - News
tags:
  - news
  - covid-19-news
  - covid-19
  - lowell
source-org: lowell-general-hospital
---

> We have received several offers to donate needed medical supplies as we plan for a potential surge of patients. Supplies most needed at hospitals across the country include personal protective equipment, primarily N95 masks, protective gowns, procedure and surgical masks, and hand sanitizer.

> While we are unable to receive outside meal and beverage deliveries at the hospital locations due to visitor restrictions and the need to reduce volume into our locations at this time to decrease the spread of the virus, we encourage those restaurants and other businesses that are interested in supporting our employees and providers to consider providing free or discounted purchases options. Some are now offering free meals or coffee to our hospital staff if they show their hospital ID badge at the time of pick up. Others are providing gift cards or discounts that we have been able to distribute to our staff for use at local establishments.

> If you are interested in supporting Lowell General Hospital in either of these ways, please email philanthropy@lowellgeneral.org and a member of our team will follow up as soon as possible.

link:https://www.lowellgeneral.org/news-and-media/news/lowell-general-hospital-is-accepting-donations[Press Release from Lowell General Hospital]