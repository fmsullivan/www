---
title: mass.gov COVID-19 Guidance and Recommendations
date: 2020-3-14 18:30:00
# updated: 2020-3-14
categories:
  - COVID-19
  - Info
tags:
  - covid-19-info
  - covid-19
source-org: mass.gov
---

link:https://mass.gov[mass.gov] has provided guidance and recommendations for local citizens.

https://www.mass.gov/info-details/covid-19-guidance-and-recommendations