---
title: COVID-19 Resources
date: 2020-3-14
# updated: 2020-3-14
categories:
    - COVID-19
---
:toc:

====
Are we missing an offer from your business, organization or non-profit?
Email us at mailto:website@lowell-mutual-aid.com[website@lowell-mutual-aid.com].
====

== Hotlines & Support Groups

- link:/covid-19/resource/parents-helping-parents/[Parents Helping Parents of Massachusetts] [State] [Non-Profit]
  Online support groups and a confidential parental stress line.

== Transportation

- link:https://docs.google.com/spreadsheets/d/1TzhQNJx4P0CWtf6j6Huv5-imG88fwosoxr3Axi6k8sI/edit#gid=688588038[MBTA Pass Donations]
  Crowdsourced project to donate/receive MBTA passes, for essential workers and those in need.

== Child Care

== Food

- link:/covid-19/resource/grab-and-go-meals/[LPS Grab and Go Meals] [Lowell] [School]
  Lowell Public Schools has announced 9 locations where students can pick up lunches.

- link:https://lbgc.org/grab-and-go-meal-service[Boys & Girls Club Grab and Go Meals]
  The Boys & Girls Club of Greater Lowell is offering lunch and dinner pick-up from 11 AM - 1 PM each weekday.

- link:https://mvfb.org/programs/mobile-pantry/[Merrimack Valley Food Bank - Mobile Pantry]
  Home delivery of nutritious foods for low-income, homebound, and disabled individuals.

=== Pet Food

- link:/covid-19/resource/humane-society/[Lowell Humane Society] [Lowell] [Non-Profit]
  The Lowell Humane Society is offering pet food for families experiencing financial hardship.

== Food and Supply Delivery

- link:/covid-19/resource/mill-city-grows/[Mill City Grows] [Lowell] [Non-Profit]
  Fresh produce orders and delivery, prioritizing low-income/high-need customers. 

== Financial Resources

- link:/covid-19/resource/mass-unemployment/[Massachusetts Unemployment claims] [State] [Government]
  Massachusetts has released information on filing unemployment claims related to COVID-19.

- link:https://www.togetherwedream.net/[Resources for Undocumented Workers and Residents] [Non-Profit]

- link:/covid-19/resource/211/[211 Service] [State] [Government]
  211 can connect people with services and resources, including people who are in need of financial assistance as a result of lost wages from event cancellations, business closures, and quarantines.

- link:https://grantstation.com/covid19fundingsopps[Grantstation Resource Roundup]
  List of resources for grant and loan resources, for small businesses and nonprofits.

- link:http://lowellma.gov/1407/COVID-19-Business-Resources[City of Lowell Business Resources]
  Includes webinars, financial resource links, and unemployment info for employers.

== Education/Learning

- link:https://www.lowell.k12.ma.us/homelearning[Lowell Public Schools Home Learning - PK-12]
  Resources from LPS teachers for all grade levels. Will be updated over time so check back!

- link:https://lowelllibrary.org/services/covid-19-suggestions-for-families/[Pollard Memorial Library - Family Resources] [Lowell] [Non-Profit]
  Roundup of many educational resources, reading options, and "social distancing for families" ideas.

- link:/covid-19/resource/lowell-makes/[Projects and Virtual Class from Lowell Makes] [Lowell] [Non-Profit]
  Virtual classes and craft ideas from Lowell Makes to reduce loneliness and provide creativity and distraction from the crisis.

- link:https://www.americanbar.org/groups/litigation/committees/childrens-rights/practice/2020/advocating-for-special-education-services-during-covid19/[Advocating for Special Education Services during COVID-19 - from American Bar Association]

== Mental Health and Social Support

- link:/covid-19/resource/sdsn/[Social Distancing Support Network (Facebook Group)]
  A group to connect with other individuals and set up virtual activities to stay social.

- link:https://www.activeminds.org/about-mental-health/be-there/coronavirus/[Active Minds] [Non-Profit]
  Webinars and resources, including some specifically for teens and young adults.

- link:https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/managing-stress-anxiety.html[CDC Mental Health and Coping]
  Information and resources for individuals, parents, caregivers, first responders and more.

- link:http://aa-intergroup.org/directory.php[AA Online Intergroup - Online Meetings Directory] [Non-Profit]
  Online Alcoholics Anonymous meetings and support, searchable by format and populations, too. 

=== Domestic / Interpersonal / Sexual Violence support services

-link:https://chhinc.org/chat/[Center for Hope and Healing - online chat, hotline, and more] [Lowell] [Greater Lowell] [Non-Profit] 

-link:https://www.frcma.org/about/covid-19[SafeLink expanded services] [Non-Profit]

== Policy and Legal

- link:http://lawyersforcivilrights.org/coronavirus[Lawyers for Civil Rights - Legal Help & Support]
  Legal info and resources related to medical, housing, employment, immigration, and more.

- link:http://lawyersforcivilrights.org/our-impact/economic-justice/legal-responses-to-covid-19[Lawyers for Civil Rights - Policy Initiative for COVID-19 Response]
  Comprehensive advocacy and ways to support policy changes, in addition to their direct assistance as above.

== Information

- link:/covid-19/resource/eoc/[City of Lowell Emergency Operation Center] [Lowell] [Government]
  The EOC can assist Lowell residents with information, resources, and updates regarding the state of coronavirus.

- link:https://www.mass.gov/how-to/apply-for-a-discounted-communications-service-through-the-lifeline-program[MA Lifeline Program for Discounted Communications] [State]
  State government benefit program for a monthly discount on one commmunications service. See details and application.

- link:https://www.internetessentials.com/apply[Comcast Internet Essentials]
  Affordable, high-speed internet for households that are eligible for public assistance and are not existing Xfinity Internet customers. Check out the link for details and how to apply.
